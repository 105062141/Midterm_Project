var Global_user = null;     //現在登入的user

function init() {
    var user_email = '';
    firebase.auth().onAuthStateChanged(function (user) {
        Global_user = user;
        var menu = document.getElementById('dynamic-menu');
        // Check user login
        if (user) {
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function() {
                    alert("Logout Successfully!");
                    console.log('Signed Out');
                }, function(error) {
                    console.error('Sign Out Error', error);
                });
            });
        
            // write 新增商品
            var goodsWrite = document.getElementById('goodsName');
            var priceWrite = document.getElementById('goodsPrice');
            var dataWrite = document.getElementById('goodsData');
            var imageUpload = document.getElementById('goodsImage');
            var outputImage = document.getElementById('outputImg'); //show the image on HTML
            var imageFile;
            var fileName;
            var goodsRef;
            var btnWrite = document.getElementById('btnUpload');

            imageUpload.addEventListener('change', function(e){
                imageFile = imageUpload.files[0];
                fileName = imageFile.name;
                //outputImage.src = URL.createObjectURL(imageFile);    //show the image on HTML
                goodsRef = firebase.storage().ref().child('goods/' + fileName);

                if(imageFile){  //show the image on HTML
                    var reader = new FileReader();      
                    reader.onload = function(event){
                        outputImage.src = event.target.result;
                        outputImage.style.display = 'block';
                    }
                    reader.readAsDataURL(e.target.files[0]); 
                }
            });

            btnWrite.addEventListener('click', e => {
                if((goodsWrite.value != "") && (priceWrite.value != "") && (imageFile)){    //test
                var uploadTask = goodsRef.put(imageFile);

                uploadTask.on('state_changed', function(snapshot){
                }, function(error){
                    console.log(error);
                }, function(){
                    var downloadURL = uploadTask.snapshot.downloadURL;
                    console.log(downloadURL);

                    firebase.database().ref('goods').push({   //database
                        seller: user.email,
                        goods: goodsWrite.value,
                        price: priceWrite.value,
                        data: dataWrite.value,
                        image: downloadURL
                    }).catch(e => console.log(e.message));
                    
                    goodsWrite.value = "";
                    priceWrite.value = "";
                    dataWrite.value = "";
                    imageUpload.value = "";
                    outputImage.src = "";
                    outputImage.style.display = 'none';
                });
                }
            });
        } else {
            // It won't show any post if not login
            var btnWrite = document.getElementById('btnUpload');
            btnWrite.addEventListener('click', e=>{
                alert("You have to LOGIN first!");
                document.location.href = "signin.html";
            })
            menu.innerHTML = "<a class='dropdown-item' href='signin.html'>Login</a>";
            document.getElementById('post_list').innerHTML = "";
        }
    });
}

// read
var str_before = '<div class="col-lg-4 wow zoomInUp card-lg-deck mb-4"><div class="card text-center border-info"><div class="card-header border-primary"><button class="btn btn-danger goDelete" onclick="goDeleteA(&#39;' ;
var str_middle = '"><div class="card-body text-info"><h3 class="card-title">';
var str_middle2 = '</p><div class="card-footer bg-transparent border-info">';
var str_after = '</div></div></div></div>';

var readGoodsRef = firebase.database().ref('goods');
// List for store posts html
var total_post = [];
// Counter for checking history post update complete
var first_count = 0;
// Counter for checking when to update new post
var second_count = 0;

readGoodsRef.once('value').then(function (snapshot) {
        /// TODO 7: Get all history posts when the web page is loaded and add listener to update new post
        ///         1. Get all history post and push to a list
        ///         2. Join all post in list to html in once
        ///         4. Add listener for update the new post
        ///         5. Push new post's html to a list
        ///         6. Re-join all post in list to html when update
        ///
        ///         Hint: When history post count is less then new post count, update the new and refresh html
        snapshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();
            var goodsName = "" + childData.goods;
            total_post[total_post.length] = str_before + goodsName + '&#39;,' + first_count + ')"><div class="btn-text">X</div></button></div><img class="card-img-top" src="' + childData.image + str_middle + childData.goods + " $" + childData.price + '</h3><p class="card-text">' + childData.data + str_middle2 + childData.seller
                                            + '<button class="btn btn-primary ml-3" data-toggle="modal" data-target="#modal_' + 25 + first_count 
                                            + '" onclick="not_login_yet()">Buy it!</button><div class="modal fade" id="modal_' + 25 + first_count + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">'
                                            + childData.goods + " $" + childData.price
                                            + '</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><img src="'
                                            + childData.image
                                            + '" height="100%" width="100%"><div class="mx-auto" style="width: 150px">數量：<input class="d-inline-block mt-2 new_amount" type="number" value="1" min="1" style="width: 100px" required></div></div><div class="modal-footer">' 
                                            + childData.seller
                                            + '<button class="btn btn-primary ml-5" onclick="want_buy_new(&#39;'
                                            //+ childData.goods + ',' + childData.price + ',' + childData.image + ',' + childData.seller
                                            + goodsName
                                            + '&#39;,' + first_count
                                            + ')">Buy it!</button><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div></div></div></div>' + str_after;
            first_count += 1;
        });
        document.getElementById('newGoodsList').innerHTML = total_post.join('');

        readGoodsRef.on('child_added', function (data) {
            second_count += 1;
            if (second_count > first_count) {
                var childData = data.val();
                var goodsName = "" + childData.goods;
                total_post[total_post.length] = str_before + goodsName + '&#39;,' + first_count + ')"><div class="btn-text">X</div></button></div><img class="card-img-top" src="' + childData.image + str_middle + childData.goods + " $" + childData.price + '</h3><p class="card-text">' + childData.data + str_middle2 + childData.seller
                                                + '<button class="btn btn-primary ml-3" data-toggle="modal" data-target="#modal_' + 25 + first_count 
                                                + '" onclick="not_login_yet()">Buy it!</button><div class="modal fade" id="modal_' + 25 + first_count + '" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true"><div class="modal-dialog modal-dialog-centered" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="exampleModalLongTitle">'
                                                + childData.goods + " $" + childData.price
                                                + '</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body"><img src="'
                                                + childData.image
                                                + '" height="100%" width="100%"><div class="mx-auto" style="width: 150px">數量：<input class="d-inline-block mt-2 new_amount" type="number" value="1" min="1" style="width: 100px" required></div></div><div class="modal-footer">' 
                                                + childData.seller
                                                + '<button class="btn btn-primary ml-5" onclick="want_buy_new(&#39;'
                                                //+ childData.goods + ',' + childData.price + ',' + childData.image + ',' + childData.seller
                                                + goodsName
                                                + '&#39;,' + first_count
                                                + ')">Buy it!</button><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div></div></div></div>' + str_after;
                document.getElementById('newGoodsList').innerHTML = total_post.join('');
            }
        });
    }).catch(e => console.log(e.message)
);

var buyAmount = document.getElementsByClassName('amount');
var buyFavor = document.getElementsByClassName('favor');
var buyStyle = document.getElementsByClassName('cookiestyle');
var buyPrice = [
    30, 30, 15,
    75, 175,
    110, 180,
    140,
    130, 130,
    160, 160, 120,
    75, 280, 250, 250, 199,
    75, 80,
    199, 
    200, 200, 150,
    120
];
var buyImages = document.getElementsByClassName('buyImg');
var buyNewAmount = document.getElementsByClassName('new_amount');

function not_login_yet() {
    if(!Global_user){
        alert("You have to LOGIN first!");
        document.location.href = "signin.html";
    }
}

function want_buy_new(goodsID, index){
    alert(goodsID + " *" + buyNewAmount[index].value + "   已加入購物車！");

    if(Global_user){
        readGoodsRef.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                if(childData.goods == goodsID){
                    // write
                    firebase.database().ref('buyer/' + Global_user.uid).push({   
                        seller: childData.seller,
                        buyer: Global_user.uid,
                        goods: childData.goods,
                        favor: "",
                        price: childData.price,
                        amount: buyNewAmount[index].value,
                        image: childData.image
                    }).catch(e => console.log(e.message));
                }
            });
        }).catch(e => console.log(e.message));
    }
}

function want_to_buy(goodsID, index) {
    alert(goodsID + " *" + buyAmount[index].value + "   已加入購物車！");
    if(Global_user){
        // write
        firebase.database().ref('buyer/' + Global_user.uid).push({
            seller: "lucy042211@gmail.com",   
            buyer: Global_user.uid,
            goods: goodsID,
            favor: "",
            price: buyPrice[index],
            amount: buyAmount[index].value,
            image: buyImages[index].src
        }).catch(e => console.log(e.message));
    }
}

function want_to_buy_2(goodsID, index, index2){     //口味
    alert(goodsID + " " + buyFavor[index2].value + " *" + buyAmount[index].value + "   已加入購物車！");
    if(Global_user){
        // write
        firebase.database().ref('buyer/' + Global_user.uid).push({   
            seller: "lucy042211@gmail.com",   
            buyer: Global_user.uid,
            goods: goodsID,
            favor: buyFavor[index2].value,
            price: buyPrice[index],
            amount: buyAmount[index].value,
            image: buyImages[index].src
        }).catch(e => console.log(e.message));
    }
}

function want_to_buy_3(index, index2, index3){      //種類+口味
    alert(buyStyle[index3].value + " " + buyFavor[index2].value + " *" + buyAmount[index].value + "   已加入購物車！");
    if(Global_user){
        // write
        firebase.database().ref('buyer/' + Global_user.uid).push({   
            seller: "lucy042211@gmail.com",   
            buyer: Global_user.uid,
            goods: buyStyle[index3].value,
            favor: buyFavor[index2].value,
            price: buyPrice[index],
            amount: buyAmount[index].value,
            image: buyImages[index].src
        }).catch(e => console.log(e.message));
    }
}

function goDeleteA(goodsID, index){
    // if(Global_user){
        readGoodsRef.once('value').then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                var childkey = childSnapshot.key;
                if(childData.goods == goodsID){
                    if(childData.seller == Global_user.email){
                        readGoodsRef.child(childkey).remove();
                        total_post[index] = "";
                        document.getElementById('newGoodsList').innerHTML = total_post.join('');
                    }
                    else{
                        alert("You can't delete the goods!");
                    }
                }
            });
        }).catch(e => console.log(e.message));
    // }
}

function notify(){
    if (Notification && Notification.permission !== "granted") {
        Notification.requestPermission(function (status) {
          if (Notification.permission !== status) {
            Notification.permission = status;
          }
        });
    }
    var button = document.getElementById('notified');
    button.addEventListener('click', function () {
    // If the user agreed to get notified
    if (Notification && Notification.permission === "granted") {
        var n = new Notification("Hi!");
    }
    // If the user hasn't told if he wants to be notified or not
    // Note: because of Chrome, we are not sure the permission property
    // is set, therefore it's unsafe to check for the "default" value.
    else if (Notification && Notification.permission !== "denied") {
        Notification.requestPermission(function (status) {
        if (Notification.permission !== status) {
            Notification.permission = status;
        }
        // If the user said okay
        if (status === "granted") {
            var n = new Notification("Hi!");
        }
        // Otherwise, we can fallback to a regular modal alert
        else {
            alert("Hi!");
        }
        });
    }
    // If the user refuses to get notified
    else {
        // We can fallback to a regular modal alert
        alert("Hi!");
    }
    });
}

window.onload = function () {
    init();
    notify();
};
