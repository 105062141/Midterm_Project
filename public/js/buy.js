var Global_user = null; 
var money = 0;
var total_buy = [];
var total_list = [];
var seller_list = [];

function init_buy() {
    firebase.auth().onAuthStateChanged(function (user){
        Global_user = user;
        var menu = document.getElementById('dynamic-menu');
        if(user){
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function() {
                    alert("Logout Successfully!");
                    console.log('Signed Out');
                }, function(error) {
                    console.error('Sign Out Error', error);
                });
            });

            // read
            var str_before = '<div class="row"><div class="col-lg-3">';
            var str_after = '</div></div>';
    
            var readBuyerRef = firebase.database().ref('buyer/' + user.uid);
            // List for store posts html
            // total_post = [];
            // total_list = [];
            // seller_list = [];
            
            // Counter for checking history post update complete
            var first_count = 0;
            // Counter for checking when to update new post
            var second_count = 0;
    
            readBuyerRef.once('value').then(function (snapshot) {
                snapshot.forEach(function (childSnapshot) {
                    var childData = childSnapshot.val();
                    var goodName = "" + childData.goods;
                    total_buy[total_buy.length] = str_before + "<p><img src='" + childData.image + "' style='display: inline' height='20%' width='20%'> " + childData.goods + "</p></div><div class='col-lg-3 my-auto'><p>" + childData.favor + "</p></div><div class='col-lg-2 my-auto'><p>* " + childData.amount + "</p></div><div class='col-lg-2 my-auto'><p>NT$ " + childData.price + "</p></div><div class='col-lg-2 my-auto'><button class='btn btn-danger d-inline deleteBtn' onclick='deleteList(&#39;" 
                                                    + goodName + "&#39;," + total_buy.length + "," + childData.amount + "," + childData.price + ")'>X 刪除</button>" + str_after;
                    money += (childData.amount * childData.price);
                    total_list[total_list.length] = goodName + " *" + childData.amount;
                    seller_list[seller_list.length] = childData.seller;
                    first_count += 1;
                });
                document.getElementById('tableItems').innerHTML = total_buy.join('');
                //total_list = total_list.join(' ');
                seller_list = seller_list.join(' ');
                document.getElementById('allMoney').innerHTML = "NT$ " + money;

                    // readBuyerRef.on('child_removed', function (snapshot) {
                    //     snapshot.forEach(function (childSnapshot) {
                    //         var childData = childSnapshot.val();
                    //         total_list[total_list.length] = childData.goods + " *" + childData.amount;
                            
                    //     });
                    // });
                    // total_list = total_list.join(' ');
                }).catch(e => console.log(e.message)
            );
        }else{
        }
    });
}

var bbname = document.getElementById('buyerName');
var ID = document.getElementById('buyerID');
var phone = document.getElementById('buyerPhone');
var getDay = new Date();
var year = getDay.getFullYear();
var month = getDay.getMonth();
var date = getDay.getDate();

function someoneBuy(){
    alert("Buy successfully!");

    // write
    firebase.database().ref('buyerInfo/' + Global_user.uid).push({
        bName: bbname.value,
        bID: ID.value,
        bPhone: phone.value,
        bList: total_list,
        bMoney: money,
        bDay: year + "/" + (month+1) + "/" + date,
        sList: seller_list
    }).catch(e => console.log(e.message));

    firebase.database().ref('buyer/' + Global_user.uid).remove();
}

function deleteList(goodsID, index, amount, price){
    var deleteRef = firebase.database().ref('buyer/' + Global_user.uid);
    deleteRef.once('value').then(function (snapshot) {
        snapshot.forEach(function (childSnapshot) {
            var childData = childSnapshot.val();
            var childKey = childSnapshot.key;
            if(childData.goods == goodsID)
                deleteRef.child(childKey).remove();
        });
    }).catch(e => console.log(e.message));

    total_buy[index] = "";
    document.getElementById('tableItems').innerHTML = total_buy.join('');
    money -= (amount * price);
    document.getElementById('allMoney').innerHTML = "NT$ " + money;


    console.log(total_list[index]);
    //total_list[index] = "";
    total_list.splice(index, 1);
    console.log(total_list);
}

window.onload = function () {
    init_buy();
};