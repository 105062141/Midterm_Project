var Global_user = null; 

function init_buy() {
    firebase.auth().onAuthStateChanged(function (user){
        Global_user = user;
        var menu = document.getElementById('dynamic-menu');
        if(user){
            user_email = user.email;
            menu.innerHTML = "<span class='dropdown-item'>" + user.email + "</span><span class='dropdown-item' id='logout-btn'>Logout</span>";
            /// TODO 5: Complete logout button event
            ///         1. Add a listener to logout button 
            ///         2. Show alert when logout success or error (use "then & catch" syntex)
            var btnLogout = document.getElementById('logout-btn');
            btnLogout.addEventListener('click', function(){
                firebase.auth().signOut().then(function() {
                    alert("Logout Successfully!");
                    console.log('Signed Out');
                }, function(error) {
                    console.error('Sign Out Error', error);
                });
            });

            // read
            var str_before = '<div class="row"><div class="col-lg-2">';
            var str_after = '</div></div>';
    
            var readInfoRef = firebase.database().ref('buyerInfo/' + user.uid);
            // List for store posts html
            var total_post = [];
    
            readInfoRef.once('value').then(function (snapshot) {
                    snapshot.forEach(function (childSnapshot) {
                        var childData = childSnapshot.val();
                        total_post[total_post.length] = str_before + "-" + childData.bDay + '</div><div class="col-lg-2">NT$ ' + childData.bMoney + '</div><div class="col-lg-8">' + childData.bList + str_after;
                    });
                    document.getElementById('tableItems').innerHTML = total_post.join('');
                }).catch(e => console.log(e.message)
            );

            // var readSellerRef = firebase.database().ref('buyerInfo').child();
            // var total_get = [];
            // readSellerRef.once('value').then(function (snapshot) {
            //         snapshot.forEach(function (childSnapshot) {
            //             var childData = childSnapshot.val();
            //             if(Global_user.uid == childData.seller){
            //                 total_get[total_get.length] = str_before + childData.bDay + '</div><div class="col-lg-2">' + childData.bName + childData.bID + childData.bPhone + '</div><div class="col-lg-2">NT$ ' + childData.bMoney + '</div><div class="col-lg-6">' + childData.bList + str_after;
            //             }
            //         });
            //         document.getElementById('sellerTable').innerHTML = total_get.join('');
            //     }).catch(e => console.log(e.message)
            // );
        }else{
        }
    });
}

window.onload = function () {
    init_buy();
};