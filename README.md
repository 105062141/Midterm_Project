# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [SS_2018 清大雄友週_SS]
* Key functions (add/delete)
    1. [product page]
    2. [shopping pipeline] 
    3. [user dashboard]
* Other functions (add/delete)
    1. [upload new goods]
    2. [delete your own goods]
    3. [remove some goods from your shopping cart]
    4. [show the total price in your shopping cart]
    5. [chrome notification]
    6. [css animation]

## Basic Components
|      Component       | Score | Y/N  |
| :------------------: | :---: | :--: |
| Membership Mechanism |  20%  |  Y   |
|     GitLab Page      |  5%   |  Y   |
|       Database       |  15%  |  Y   |
|         RWD          |  15%  |  Y   |
|  Topic Key Function  |  15%  |  Y   |

## Advanced Components
|      Component      | Score | Y/N  |
| :-----------------: | :---: | :--: |
| Third-Party Sign In | 2.5%  |  Y   |
| Chrome Notification |  5%   |  Y   |
|  Use CSS Animation  | 2.5%  |  Y   |
|   Security Report   |  5%   |  Y   |
|   Other functions   | 1~10% |  Y   |

## Website Detail Description

- 首頁(index.html)：當首頁點選 navbar 可以連結到其他頁面，或直接 scroll 至相對應的功能區塊。會跳出 chrome notification。
  - 最新商品區：可動態即時 show 出新增的商品資訊及圖片，點選 buy it 可選擇數量，並加入購物車；按 “X” 可刪除自己所新增的商品(不能刪除別人的)，show 出商品的時候有動畫。
  - 預購商品區：可選擇商品數量及口味，加入購物車，show 出商品的時候有動畫。
  - 新增商品區：輸入商品資訊及商品照片，當商品照片 load  成功之後可以即時預覽，以確保所選的圖片是對的。
- 登入頁面(signin.html)：可選擇 google 登入或者 email 登入，email 登入前須先創一個帳號。
- 購物車(buy.html)：show 出所點選購買的商品資訊，包含商品圖片、名稱、口味、數量、及單價，若不想買此樣商品也可以點選刪除，此樣商品就會移出購物車。最後算出總價錢後，按下結帳，會跳出購買者資訊，輸入完畢之後按確認，即可送出訂單。
- 訂單查詢(buylist.html)：show出訂單們的訂購日期、訂單總價、及購買明細等等訂單記錄，且只能查詢自己的訂單。

## Security Report (Optional)

* 刪除商品：只能刪除自己新增的商品，無法刪除別人的。
* 訂單查詢：只能查詢自己的訂單。